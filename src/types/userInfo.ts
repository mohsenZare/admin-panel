interface UserInfo {
  blockedUserList: string[]
  contactOnlyFor: string[]
  createdAt: string
  deleted: boolean
  fullName: string
  hideDoneSubTask: boolean
  isActive: boolean
  isOnline: boolean
  mirrorFromOthers: boolean
  mobile: string
  mutedUserList: string[]
  recommenderUser: boolean
  robot: boolean
  syncContacts: boolean
  taskBadges: TaskBadges
  tasksVisibility: string
  username: string
  _id: string
}

interface TaskBadges {
  list: boolean
  time: boolean
  assignee: boolean
  priority: boolean
  subTask: boolean
  description: boolean
  dueDate: boolean
  feeling: boolean
  note: boolean
  privateExpressions: boolean
  rightness: boolean
  startDate: boolean
  status: boolean
  tags: boolean
  taskColor: string
}
