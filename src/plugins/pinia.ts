import { App } from 'vue'
import { createPinia } from 'pinia'
import { createPersistedState } from 'pinia-plugin-persistedstate'

const piniaPlugin = {
  install: (app: App) => {
    const pinia = createPinia()
    pinia.use(
      createPersistedState({
        storage: localStorage
      })
    )
    app.use(pinia)
  }
}

export default piniaPlugin
