import { App } from 'vue'
import { createVuetify } from 'vuetify'
import type { ThemeDefinition } from 'vuetify'
import 'vuetify/styles'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import '@/assets/styles/global.scss'
const lightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    primary: '#67BFBB',
    background: '#FFFFFF',
    color: '#314448',
    second: '#F8F8F8',
    danger: '#FC6D69',
    warning: '#FFFF00'
  }
}
const darkTheme: ThemeDefinition = {
  dark: true,
  colors: {
    primary: '#67BFBB',
    background: '#314448',
    color: '#FFFFFF',
    second: '#27373b',
    danger: '#FC6D69',
    warning: '#FFFF00',
    success: '#67c4c1'
  }
}

const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'dark',
    themes: {
      light: lightTheme,
      dark: darkTheme
    }
  }
})

const vuetifyPlugin = {
  install: (app: App) => {
    app.use(vuetify)
  }
}

export default vuetifyPlugin
