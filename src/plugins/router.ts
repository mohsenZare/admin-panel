import { App } from 'vue'
import router from '@/router/router'

const routerPlugin = {
  install: (app: App) => {
    app.use(router)
  }
}

export default routerPlugin
