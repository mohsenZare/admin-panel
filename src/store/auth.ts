import { defineStore } from 'pinia'
import axios from '@/services/axios'
import router from '@/router/router'
import type { UserInfo } from '@/types/userInfo'

interface UserData {
  accessToken: string | null
  refreshToken: string | null
  user: UserInfo | null
}

export const useAuthStore = defineStore({
  id: 'auth',
  state: (): UserData => ({
    accessToken: null,
    refreshToken: null,
    user: null
  }),

  persist: true,
  getters: {
    isAuthenticated(): boolean {
      return !!this.accessToken
    },
    getAccessToken(): string | null {
      return this.accessToken
    },
    getUserInfo(): UserInfo | null {
      return this.user
    }
  },
  actions: {
    async login(mobile: string, password: string) {
      try {
        const res = await axios.post<UserData>('auth/login', {
          mobile,
          password,
          syncContacts: true
        })
        this.setTokens(res.data)
        this.setUserProfile(res.data.user)
        router.push('/panel')
      } catch (error) {
        throw error
      }
    },
    async sendRefreshToken() {
      try {
        const res = await axios.post<UserData>('auth/refresh', {
          refreshToken: this.refreshToken
        })
        this.setTokens(res.data)
      } catch (error) {
        throw error
      }
    },
    async logout() {
      try {
        await axios.post('auth/logout')
        this.clearTokens()
        router.push('/')
      } catch (error) {
        throw error
      }
    },
    setTokens(data: UserData) {
      this.refreshToken = data.refreshToken
      this.accessToken = data.accessToken
    },
    clearTokens() {
      this.refreshToken = null
      this.accessToken = null
      this.user = null
    },
    setUserProfile(user: any) {
      this.user = user
    }
  }
})
