import { createRouter, createWebHistory } from 'vue-router'
import Login from '@/views/Login.vue'
import Panel from '@/views/Panel.vue'
import NotFound from '@/views/404.vue'
import setupRouterMiddleware from './routerMiddleware'

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: { redirectIfLoggedIn: true }
  },
  {
    path: '/panel',
    name: 'panel',
    component: Panel,
    meta: { requiresAuth: true }
  },
  {
    path: '/:catchAll(.*)', // Match any path
    name: 'NotFound',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

setupRouterMiddleware(router)

export default router
