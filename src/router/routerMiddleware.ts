import {
  Router,
  NavigationGuardNext,
  RouteLocationNormalized
} from 'vue-router'
import authService from '@/services/authService'

const beforeEachGuard = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
) => {
  if (to.meta.requiresAuth) {
    if (authService.isAuthenticated()) {
      next()
    } else {
      next({ name: 'login' })
    }
  } else if (to.meta.redirectIfLoggedIn) {
    if (authService.isAuthenticated()) {
      next({ name: 'panel' })
    } else {
      next()
    }
  } else {
    next()
  }
}

export default function setupRouterMiddleware(router: Router) {
  router.beforeEach(beforeEachGuard)
}
