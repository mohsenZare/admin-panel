import { useAuthStore } from '@/store/auth'

const authService = {
  isAuthenticated() {
    return useAuthStore().isAuthenticated
  },
  login(mobile: string, password: string) {
    return useAuthStore().login(mobile, password)
  },
  refreshToken() {
    return useAuthStore().sendRefreshToken()
  },
  logout() {
    return useAuthStore().logout()
  },
  getUserInfo() {
    return useAuthStore().getUserInfo
  }
}

export default authService
