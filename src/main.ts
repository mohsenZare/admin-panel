//imports
import { createApp } from 'vue'
import App from './App.vue'
import router from '@/plugins/router'
import vuetify from '@/plugins/vuetify'
import pinia from '@/plugins/pinia'
///variables
const app = createApp(App)

//uses
app.use(router)
app.use(vuetify)
app.use(pinia)

///mount
app.mount('#app')
